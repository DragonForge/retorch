package com.zeitheron.retorch;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.block.Block;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@EventBusSubscriber
@Mod(modid = "retorch", name = "Retorch", version = "@VERSION@", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97", updateJSON = "https://gitlab.com/DragonForge/retorch/raw/master/latest.json?inline=false")
public class ReTorch
{
	public static final Logger LOG = LogManager.getLogger("Retorch");
	
	@SidedProxy(serverSide = "com.zeitheron.retorch.CommonProxy", clientSide = "com.zeitheron.retorch.ClientProxy")
	public static CommonProxy proxy;
	
	@SubscribeEvent
	public static void onBlockRegistry(RegistryEvent.Register<Block> e)
	{
		ModContainer minecraft = Loader.instance().getMinecraftModContainer();
		ModContainer active = Loader.instance().activeModContainer();
		
		List<Block> blocks = new ArrayList<>();
		
		LOG.info("************************");
		LOG.info("REGISTERING FIXES FOR VANILLA");
		LOG.info("************************");
		
		try
		{
			Loader.instance().setActiveModContainer(minecraft);
			
			{
				BlockTorchFixed fixed = new BlockTorchFixed();
				fixed.setRegistryName("minecraft", "torch");
				blocks.add(fixed);
				LOG.info("Fixed minecraft:torch.");
			}
		} finally
		{
			Loader.instance().setActiveModContainer(active);
		}
		
		blocks.forEach(e.getRegistry()::register);
	}
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with SolarFluxReborn jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://minecraft.curseforge.com/projects/309441 !");
		LOG.warn("*****************************");
		
		try
		{
			Class HammerCore = Class.forName("com.zeitheron.hammercore.HammerCore");
			Map<String, String> invalidCertificates = (Map<String, String>) HammerCore.getDeclaredField("invalidCertificates").get(null);
			invalidCertificates.put("retorch", "https://minecraft.curseforge.com/projects/309441");
		} catch(Throwable err)
		{
			if(err instanceof ClassNotFoundException)
				return;
			if(err instanceof NoClassDefFoundError)
				return;
			err.printStackTrace();
		}
	}
}