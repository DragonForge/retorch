package com.zeitheron.retorch;

import net.minecraft.block.Block;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockTorchFixed extends BlockTorch
{
	{
		setTranslationKey("torch");
		setSoundType(SoundType.WOOD);
		setLightLevel(0.9375F);
	}
	
	@Override
	protected boolean checkForDrop(World worldIn, BlockPos pos, IBlockState state)
	{
		if(!this.canPlaceAt$(worldIn, pos, state.getValue(FACING)))
			for(EnumFacing enumfacing : FACING.getAllowedValues())
				if(this.canPlaceAt$(worldIn, pos, enumfacing))
				{
					worldIn.setBlockState(pos, state.withProperty(FACING, enumfacing));
					return false;
				}
		return super.checkForDrop(worldIn, pos, state);
	}
	
	private boolean canPlaceAt$(World worldIn, BlockPos pos, EnumFacing facing)
	{
		BlockPos blockpos = pos.offset(facing.getOpposite());
		IBlockState iblockstate = worldIn.getBlockState(blockpos);
		Block block = iblockstate.getBlock();
		BlockFaceShape blockfaceshape = iblockstate.getBlockFaceShape(worldIn, blockpos, facing);
		
		if(facing.equals(EnumFacing.UP) && this.canPlaceOn$(worldIn, blockpos))
		{
			return true;
		} else if(facing != EnumFacing.UP && facing != EnumFacing.DOWN)
		{
			return !isExceptBlockForAttachWithPiston(block) && blockfaceshape == BlockFaceShape.SOLID;
		} else
		{
			return false;
		}
	}
	
	private boolean canPlaceOn$(World worldIn, BlockPos pos)
	{
		IBlockState state = worldIn.getBlockState(pos);
		return state.getBlock().canPlaceTorchOnTop(state, worldIn, pos);
	}
}