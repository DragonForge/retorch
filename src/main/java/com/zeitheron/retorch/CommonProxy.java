package com.zeitheron.retorch;

import net.minecraftforge.common.MinecraftForge;

public class CommonProxy
{
	{
		MinecraftForge.EVENT_BUS.register(this);
	}
}